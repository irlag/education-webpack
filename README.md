### Build for production
```bash
$ yarn build
```

### Dev server start
```bash
$ yarn start
```
