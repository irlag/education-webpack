const webpack = require('webpack');
const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

let config = {
    context: path.resolve(__dirname, './src/js'),
    entry: {
        app: './app',
        vendor: ['jquery', 'lodash']
    },
    output: {
        //path: NODE_ENV == 'development' ? path.resolve(__dirname, './public') : path.resolve(__dirname, './dist'),
        path: path.resolve(__dirname, './dist'),
        filename: NODE_ENV == 'development' ? '[name].js' : '[name].[hash].js'
        //filename: '[name].[hash].js'
    },
    resolve: {
        extensions: ['.js'],
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules')
        ],
        alias: {
            Components: path.resolve(__dirname, 'src/components/')
        }
    },
    //devtool: "cheap-module-source-map"
    devtool: NODE_ENV == 'development' ? 'source-map' : false,
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015'],
                            plugins: ['transform-runtime']
                        }
                    }
                ]
            },
            {
                test: /\.(scss|sass)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css-loader',
                        'postcss-loader',
                        'sass-loader'
                    ]
                })
            },
            {
                test: /\.css$/,
                exclude: /(node_modules)/,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css-loader'
                        //'postcss-loader',
                    ]
                })
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                import: false,
                                url: false
                            }
                        }

                    ]
                })
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images/'
                            //publicPath: '/assets/'
                        }
                    }
                ]
            },
            {
                test: /\.(eot|woff|ttf|svg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/'
                            //publicPath: '/assets/'
                        }
                        /*loader: 'url-loader',
                        options: {
                            name: '[path][name].[ext]',
                            limit: 30000
                        }*/
                    }
                ]
            },
            {
                test: /\.hbs/,
                loader: 'handlebars-loader',
                exclude: /(node_modules|bower_components)/
            }
            /*{
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        //attrs: [':data-src'],
                        interpolate: 'require'
                    }
                }
            }*/
            //{test: /\.json$/, loader: 'json-loader'},
        ],
        noParse: /jquery|lodash/
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        /*new webpack.EnvironmentPlugin([
            'NODE_ENV'
        ]),*/
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            _: 'lodash'
            //pluck: 'lodash/collection/pluck'
        }),
        new CopyWebpackPlugin([
            {
                from: '../images/*',
                to: 'images/'
            }
            /*{
                from: '../html*//*',
                to: '[name].[ext]'
            }*/
        ]),
        new HtmlWebpackPlugin({
            template: '../template/index.template.hbs',
            //chunks: ['app'],
            title: 'Webpack 2'
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    devServer: {
        //contentBase: path.resolve(__dirname, './public'),
        //open: true
    }
};

if (NODE_ENV == 'production') {
    config.plugins.push(
        new ExtractTextPlugin({
            filename: '[name].[contenthash].css',
            allChunks: true
        }),
        new AssetsPlugin({
            filename: 'assets.json',
            path: path.resolve(__dirname, './dist')
        }),
        new ImageminPlugin({
            test: /\.(jpe?g|png|gif)$/
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            },
            compress: {
                screw_ie8: true,
                warnings: false,
                drop_console: false,
                unsafe: true
            },
            comments: false
        })
    );
} else {
    config.plugins.push(
        new ExtractTextPlugin({
            filename: '[name].css',
            allChunks: true
        }),
        new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            host: 'localhost',
            port: 3000,
            proxy: 'localhost:8080',
            open: true
            //server: { baseDir: ['public'] }
        })
    );
}

module.exports = config;