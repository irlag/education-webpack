let config = {
    plugins: [
        require('postcss-inline-svg')({
            path: './src/images/svg'
        }),
        require('autoprefixer')(
            {
                browsers: [
                    'last 5 versions'
                ]
            }
        )
    ]
};

if (process.env.NODE_ENV == 'production') {
    config.plugins.push(
        require('cssnano')({
            preset: 'default'

        })
    );
}

module.exports = config;