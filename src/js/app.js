import 'r-ulybka-ui-kit';
import '../scss/main.scss';

import {Product} from 'Components/Product';
import User from 'Components/User';

let product = new Product();
product.addToBasket();

const Promise = require("bluebird");

let user = new User('Нестор', 'Северов');
user.sayHi();

const resolve = console.log,
    reject = console.log;

$('#set-name').on('click', function() {
    new Promise(function() {
        var result = window.prompt('What is your name?');

        if (result != null) {
            user.fullName = name;
            resolve(result);
        } else {
            reject(new Error('User cancelled'));
            //throw new Error('User cancelled');
        }
    })
        .then(resolve)
        .catch(reject);
});

const sendBasketBtn = $('#send-ajax');
sendBasketBtn.on('click', function() {
    sendBasketBtn.prop('disabled', true);
    new Promise(function(resolve, reject) {
        $.ajax({
            url: '/',
            //dataType: 'json',
            data: {},
            type: 'GET',
            success: function(data) {
                sendBasketBtn.prop('disabled', false);
                resolve(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                sendBasketBtn.prop('disabled', false);
                reject(jqXHR.status + ' ' + textStatus + ' ' + errorThrown);
            }
        });
    })
        .then(resolve)
        .catch(reject);
});

if (NODE_ENV == 'production') {
    console.log("I am in production");
} else {
    console.log("I am in dev");
}
