class User {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`;
    }

    set fullName(newValue) {
        [this.firstName, this.lastName] = newValue.split(' ');
    }

    sayHi() {
        console.log("Привет, я " + this.fullName);
    }
}

export default User;